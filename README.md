# PHY2215 - Thermodynamique
## Projet num�rique - Partie 2: Le mod�le Ising

# Comment l'utiliser:
1. Installer rust sur votre ordinateur (https://www.rust-lang.org/).
2. T�l�charger le project (avec git clone par exemple).
3. Dans le terminal, naviguer vers la racine du projet (le dossier phy2215_project/) et ex�cuter la commande:
~~~bash
cargo run --release
~~~
Le programme sera alors compil� (la premi�re fois �a peut prendre plusieurs minutes) et ex�cut�. L'ex�cution peut prendre quelques dizaines de secondes avec les param�tres par d�faut ou quelques heures pour un r�seau de 500 x 500 particules.

Les fichiers images de sortie seront g�n�r�s dans le dossier plotters-doc-data/

Les diff�rents param�tres du programme sont les constantes �crites en majuscules au d�but du fichier source (src/main.rs):
~~~rust
const STOP: usize = 2_000;
const BURN_IN_STOP: usize = 1_000;
const REDUNDANCY: usize = 4; // Number of new seed state per temperature
const MAGNETIC_FIELD: f64 = 0.8; // B
const MAGNETIC_MOMENT: f64 = 1.0; // mu
const INTERACTION_ENERGY: f64 = 1.0; // J
const SIZE: usize = 5; // Size of the (square) states
const MIN_BETA: f64 = 1.0/128.0;
const MAX_BETA: f64 = 10.0;
~~~

# R�sultats
D'autres r�sultats sont disponibles dans le dossier plotters-doc-data/.

Pour des petits r�seaux, on peut voir qu'en refroidissant certaines configurations deviennent stables m�me si elles ne sont pas � l'�nergie minimale. Ce qui produit les phases dans les graphiques d'�nergie. Afin de mieux voir cet effet, pour chaque temp�rature le programme fait l'algo de Metropolis-Hastings quatre fois, avec un �tat de d�part compl�tement (ind�pendamment) diff�rent de l'essai pr�c�dent (voir le param�tre REDUNDANCY).

On peut voir aussi qu'aux temp�ratures o� on a un changement de phase, les �tats ne sont pas stables pendant l'algo M-H (i.e. on accepte souvent l'�tat propos�) et on obtient alors des valeurs interm�diaires pour les �nergies.

## 5 x 5 particules, B = 0.8
### �nergie moyenne
![Alt text](plotters-doc-data/SIZE=05/energy_mean.png)

### Susceptibilit� magn�tique (chi)
![Alt text](plotters-doc-data/SIZE=05/magnetic_susceptibility.png)

### Aimantation moyenne
![Alt text](plotters-doc-data/SIZE=05/magnetization_mean.png)

### Capacit� thermique � volume constant
![Alt text](plotters-doc-data/SIZE=05/specific_heat.png)

## 5 x 5 particules encore, mais sans champ magn�tique externe (B = 0)
### �nergie moyenne
![Alt text](plotters-doc-data/SIZE=05_B=0/energy_mean.png)

### Suceptibilit� magn�tique (chi)
![Alt text](plotters-doc-data/SIZE=05_B=0/magnetic_susceptibility.png)

### Aimantation moyenne
![Alt text](plotters-doc-data/SIZE=05_B=0/magnetization_mean.png)

### Capacit� thermique � volume constant
![Alt text](plotters-doc-data/SIZE=05_B=0/specific_heat.png)

## 5 x 5 particules encore, mais avec champ magn�tique externe invers� (B = -0.6)
### �nergie moyenne
![Alt text](plotters-doc-data/SIZE=05_B=-0.6/energy_mean.png)

### Suceptibilit� magn�tique (chi)
![Alt text](plotters-doc-data/SIZE=05_B=-0.6/magnetic_susceptibility.png)

### Aimantation moyenne
![Alt text](plotters-doc-data/SIZE=05_B=-0.6/magnetization_mean.png)

### Capacit� thermique � volume constant
![Alt text](plotters-doc-data/SIZE=05_B=-0.6/specific_heat.png)

## 500 x 500 particules, B = 0.8
### �nergie moyenne
![Alt text](plotters-doc-data/SIZE=500/energy_mean.png)

### Suceptibilit� magn�tique (chi)
![Alt text](plotters-doc-data/SIZE=500/magnetic_susceptibility.png)

### Aimantation moyenne
![Alt text](plotters-doc-data/SIZE=500/magnetization_mean.png)

### Capacit� thermique � volume constant
![Alt text](plotters-doc-data/SIZE=500/specific_heat.png)