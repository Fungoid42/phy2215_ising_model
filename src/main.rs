// Ising model using the Metropolis-Hastings algorithm

use plotters::prelude::*;
use rand::prelude::*;
//use image::{ImageBuffer, GrayImage};
//use rand_distr::{Normal, Distribution};


// Parameters
const STOP: usize = 2_000;
const BURN_IN_STOP: usize = 1_000;
const REDUNDANCY: usize = 4; // Number of new seed state per temperature
const MAGNETIC_FIELD: f64 = 0.8; // B
const MAGNETIC_MOMENT: f64 = 1.0; // mu
const INTERACTION_ENERGY: f64 = 1.0; // J
const SIZE: usize = 5; // Size of the (square) states
const MIN_BETA: f64 = 1.0/128.0;
const MAX_BETA: f64 = 10.0;


// This function takes a state and calculates its neighbourhood sum (sum_{<ij>} sigma_i * sigma_j)
// and spin sum (sum_{i} sigma_i)
fn sums(state: [[i8; SIZE]; SIZE]) -> (isize, isize) {
  let mut hood_sum: isize = 0;
  let mut spin_sum: isize = 0; 

  for i in 0..SIZE {
    for j in 0..SIZE {
      hood_sum += (state[i][j] * (state[(i+1)%SIZE][j] + state[(i+1)%SIZE][(j+1)%SIZE] 
                                  + state[i][(j+1)%SIZE] + state[(i+SIZE-1)%SIZE][(j+1)%SIZE])) as isize;
      spin_sum += state[i][j] as isize;
    }
  }

  (hood_sum, spin_sum)
}


// This function uses the hood and spin sums to calculate the energy
fn energy(hood_sum: isize, spin_sum: isize) -> f64 {
  -1.0*INTERACTION_ENERGY*(hood_sum as f64) - MAGNETIC_MOMENT*MAGNETIC_FIELD*(spin_sum as f64)
}


// This function uses the spin sum to calculate the magnetization
fn magnetization(spin_sum: isize) -> f64 {
  MAGNETIC_MOMENT * (spin_sum as f64) / ((SIZE*SIZE) as f64)
}


// This function uses the current state to proposes a new state
fn new_prop_state(current_state: [[i8; SIZE]; SIZE]) -> [[i8; SIZE]; SIZE] {
  let mut prop_state = current_state;
  
  let mut rng = thread_rng();

  let x: usize = rng.gen_range(0, SIZE);
  let y: usize = rng.gen_range(0, SIZE);
  prop_state[x][y] *= -1;

  prop_state
}


// Produce a plot of the data, in this case of the different interesting values over the coldness beta
fn plot_point(title: String, x_desc: String, y_desc: String, data: &Vec<[f64; 2]>, extrema: [f64; 2])
-> Result<(), Box<dyn std::error::Error>> {
  let file_name = format!("plotters-doc-data/{}.png", title);

  let root =
      BitMapBackend::new(&file_name, (1920, 1080)).into_drawing_area();

  root.fill(&WHITE)?;

  let mut chart = ChartBuilder::on(&root)
      .margin(10)
      .caption(
          title,
          ("sans-serif", 40),
      )
      .set_label_area_size(LabelAreaPosition::Left, 60)
      .set_label_area_size(LabelAreaPosition::Right, 60)
      .set_label_area_size(LabelAreaPosition::Bottom, 40)
      .build_ranged(
          MIN_BETA..MAX_BETA,
          extrema[0]..extrema[1],
      )?;

  chart
      .configure_mesh()
      .x_labels(30)
      .x_desc(x_desc)
      .y_desc(y_desc)
      .x_label_formatter(&|x| format!("{:.3}", 1.0 / *x))
      .y_label_formatter(&|x| format!("{:}", *x))
      .draw()?;

  chart.draw_series(
      data
      .iter().map(|[x, y]| Circle::new((*x, *y), 3, BLUE.filled())),
  )?;


  Ok(())
}


fn main() -> Result<(), Box<dyn std::error::Error>> {
  let mut current_state: [[i8; SIZE]; SIZE] = [[1; SIZE]; SIZE];

  let mut energy_mean_record: Vec<[f64; 2]> = Vec::with_capacity(1000);
  let mut energy_extrema: [f64; 2] = [0.0, 0.0];

  let mut magnetization_mean_record: Vec<[f64; 2]> = Vec::with_capacity(1000);
  let mut magnetization_extrema: [f64; 2] = [0.0, 0.0];

  let mut specific_heat_record: Vec<[f64; 2]> = Vec::with_capacity(1000); // C_V (at constant volume)
  let mut specific_heat_extrema: [f64; 2] = [0.0, 0.0];

  let mut magnetic_susceptibility_record: Vec<[f64; 2]> = Vec::with_capacity(1000);
  let mut magnetic_susceptibility_extrema: [f64; 2] = [0.0, 0.0];

  let n = (STOP - BURN_IN_STOP) as f64;


  // Thermodynamic beta (coldness = 1 / temperature)
  // WARNING: Must always be positive
  let mut coldness: f64 = MIN_BETA;

  while coldness <= MAX_BETA {
    for _s in 0..REDUNDANCY {
      // Initial random state
      for i in 0..SIZE {
        for j in 0..SIZE {
          if rand::random() {
            current_state[i][j] *= -1;
          }
        }
      }

      let mut energy_sum: f64 = 0.0;
      let mut energy_square_sum: f64 = 0.0;
      
      let mut magnetization_sum: f64 = 0.0;
      let mut magnetization_square_sum: f64 = 0.0;
  
      // Iterate
      for i in 0..STOP {
        // Random candidate
        let prop_state = new_prop_state(current_state);
  
        let (current_hood_sum, current_spin_sum) = sums(current_state);
        let (prop_hood_sum, prop_spin_sum) = sums(prop_state);
  
        let prop_energy = energy(prop_hood_sum, prop_spin_sum);
        let current_energy = energy(current_hood_sum, current_spin_sum);
  
        let prop_magnetization = magnetization(prop_spin_sum);
        let current_magnetization = magnetization(current_spin_sum);
  
        let delta_e: f64 = prop_energy - current_energy;
  
        if delta_e > 0.0 {
          // Acceptance probability
          let a: f64 = std::f64::consts::E.powf(-1.0*coldness*delta_e);
  
          // Accept or reject
          let u: f64 = rand::thread_rng().gen();
  
          if u <= a { // Accept
            current_state = prop_state;
            if i >= BURN_IN_STOP {
              energy_sum += prop_energy;
              energy_square_sum += prop_energy * prop_energy;

              magnetization_sum += prop_magnetization;
              magnetization_square_sum += prop_magnetization * prop_magnetization;
            }
          } else { // Reject
            if i >= BURN_IN_STOP {
              energy_sum += current_energy;
              energy_square_sum += current_energy * current_energy;

              magnetization_sum += current_magnetization;
              magnetization_square_sum += current_magnetization * current_magnetization;
            }
          }
        } else { // Accept
          // In this case, the energy of the proposed state is lower than the energy of the current state,
          // so we automatically accept the proposed state
          current_state = prop_state;
          if i >= BURN_IN_STOP {
            energy_sum += prop_energy;
            energy_square_sum += prop_energy * prop_energy;

            magnetization_sum += prop_magnetization;
            magnetization_square_sum += prop_magnetization * prop_magnetization;
          }
        }
      }
  
      // Collecting data from the current state
      let energy_mean = energy_sum / n;
      let energy_square_mean = energy_square_sum / n;

      let magnetization_mean = magnetization_sum / n;
      let magnetization_square_mean = magnetization_square_sum / n;

      let specific_heat = coldness*coldness * (energy_square_mean - energy_mean*energy_mean) / n;

      let magnetic_susceptibility = coldness * (magnetization_square_mean - magnetization_mean*magnetization_mean);
  
      // Keeps track of the extrema
      if magnetization_mean < magnetization_extrema[0] {
        magnetization_extrema[0] = magnetization_mean;
      } else if magnetization_mean > magnetization_extrema[1] {
        magnetization_extrema[1] = magnetization_mean;
      }
      if energy_mean < energy_extrema[0] {
        energy_extrema[0] = energy_mean;
      } else if energy_mean > energy_extrema[1] {
        energy_extrema[1] = energy_mean;
      }
      if specific_heat < specific_heat_extrema[0] {
        specific_heat_extrema[0] = specific_heat;
      } else if specific_heat > specific_heat_extrema[1] {
        specific_heat_extrema[1] = specific_heat;
      }
      if magnetic_susceptibility < magnetic_susceptibility_extrema[0] {
        magnetic_susceptibility_extrema[0] = magnetic_susceptibility;
      } else if magnetic_susceptibility > magnetic_susceptibility_extrema[1] {
        magnetic_susceptibility_extrema[1] = magnetic_susceptibility;
      }
  
      energy_mean_record.push([coldness, energy_mean]);
      magnetization_mean_record.push([coldness, magnetization_mean]);
      specific_heat_record.push([coldness, specific_heat]);
      magnetic_susceptibility_record.push([coldness, magnetic_susceptibility]);

      /*
      // Image buffer
      let mut img: GrayImage = ImageBuffer::new(SIZE as u32, SIZE as u32);

      // Writing the image
      for (x, y, pixel) in img.enumerate_pixels_mut() {
        if current_state[x as usize][y as usize] == 1 {
          *pixel = image::Luma([255u8]);
        } else {
          *pixel = image::Luma([0u8]);
        }
      }

      // Write the contents of this image to the Writer in PNG format.
      img.save(format!("final_states/beta={:.4}_e={:.4}_m={:.4}.png", coldness, energy_mean, magnetization_mean)).unwrap();
      */
    }

    coldness += 1.0/128.0;
  }

  // Plot the data
  plot_point("energy_mean".to_string(), "temperature".to_string(), "energy_mean".to_string(), &energy_mean_record, energy_extrema)?;
  plot_point("magnetization_mean".to_string(), "temperature".to_string(), "magnetization_mean".to_string(), &magnetization_mean_record, magnetization_extrema)?;
  plot_point("specific_heat".to_string(), "temperature".to_string(), "specific_heat".to_string(), &specific_heat_record, specific_heat_extrema)?;
  plot_point("magnetic_susceptibility".to_string(), "temperature".to_string(), "magnetic_susceptibility".to_string(), &magnetic_susceptibility_record, magnetic_susceptibility_extrema)?;

	Ok(())
}
